<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\BaseController as BaseController;
use function Ramsey\Collection\Map\get;
use function Symfony\Component\Mime\Header\all;

class UserController extends Controller
{
    public $successStatus = 200;

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'error' => $validator->errors()
                ], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
//        dd($user->createToken('MyApp')->accessToken->token);
        $success['token'] = $user->createToken('MyApp')->accessToken->token;
        $success['name'] = $user->name;

        return response()->json(
            [
                'success' => $success
            ],
            $this->successStatus
        );
    }

    public function login(Request $request)
    {

//        if (Auth::attempt(
//            [
//                'email' => request('email'),
//                'password' => request('password')
//            ]
//        )) {
//            $user = Auth::user();
//            $success['token'] = $user->createToken('MyApp')->accessToken->token;
//
//            return response()->json(
//                [
//                    'success' => $success
//                ],
//                $this->successStatus
//            );
//        } else {
//            return response()->json(
//                [
//                    'error' => 'Unauthorised'
//                ], 401);
//        }
//        dd($request -> get('email', 'password'));
        if (Auth::attempt([

            'email' => $request->get('email'),
            'password' => $request->get('password')
        ])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken->token;

            return response()->json(
                [
                    'success' => $success
                ],
                $this->successStatus
            );
        } else {
            return response()->json(
                [
                    'error' => 'Unauthorised'
                ], 401);
        }
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(
            [
                'success' => $user
            ],
            $this->successStatus
        );
    }

    public function search(Request $request, $email)
    {
        $result = User::where("email", "like", "%$email%")
            ->orwhere("name", "like", "%$email%")
            ->orderBy('id', 'desc')
            ->get();
        if (count($result) == 0) {
            return response()->json([
                'error' => 'not find'
            ], 401);

        } else {
            return response()->json([
                'data' => $result
            ], 200);
        }

    }
}
